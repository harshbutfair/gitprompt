use git2::Repository;
use git2::Status;
use git2::StatusOptions;

use ansi_term::Colour::{Red, Green, Cyan};

fn main() {
    // Call Repository::discover on the current directory. The return type
    // is Result, which can be Ok (encapsulating the returned Repository object)
    // or Err (encapsulating an error message).
    if let Ok(repo) = Repository::discover(".") {
        // If the result is Ok, print the branch name and status. The
        // Repository is passed to both methods by const reference.
        print_branch_name(&repo);
        print_status(&repo);
    }
    // If the result is Err, we are not in a git repository, so do nothing.
}

/// Print the branch name if HEAD is available, otherwise do nothing.
fn print_branch_name(repo: &Repository)
{
    // Similar to the main method - if we successfully retrieve git HEAD then
    // print the branch name, otherwise do nothing.
    if let Ok(head) = repo.head() {
        // head.shorthand() returns an Option (conceptually similar to C++
        // std::optional). The unwrap_or method returns the contained value
        // (if available) or the given value otherwise.
        print!("({})  ", head.shorthand().unwrap_or("<ERROR>"));
    }
    // if failed to get HEAD, don't print branch name
}

/// Print the current status of changed files in the current repo. Staged
/// and unstaged changes are printed in a different colour. If there are
/// no changes, nothing is printed.
fn print_status(repo: &Repository)
{
    let mut status_options = StatusOptions::new();
    status_options.include_ignored(false);
    status_options.include_untracked(true);
    status_options.include_unmodified(false);
    status_options.exclude_submodules(false);
    status_options.recurse_untracked_dirs(false);
    status_options.recurse_ignored_dirs(false);
    status_options.renames_from_rewrites(false);
    status_options.renames_head_to_index(true);
    status_options.renames_index_to_workdir(true);

    // statuses takes an Option<StatusOptions>
    // To convert status_options to an Option value, wrap it in
    // Some (as opposed to None).
    let status = repo.statuses(Some(&mut status_options));
    // status is a Result.
    if let Ok(status) = status {
        // Iterate though each of the returned status values and increment
        // the relevant counters. There is also a "typechange" status but
        // I don't know what that means.
        let mut staged_new = 0;
        let mut staged_mod = 0;
        let mut staged_del = 0;
        let mut staged_ren = 0;
        let mut unstaged_new = 0;
        let mut unstaged_mod = 0;
        let mut unstaged_del = 0;
        let mut unstaged_ren = 0;
        let mut conflicts = 0;

        for s in status.iter()
        {
            // Staged files.
            increment(&mut staged_new, s.status(), Status::INDEX_NEW);
            increment(&mut staged_mod, s.status(), Status::INDEX_MODIFIED);
            increment(&mut staged_del, s.status(), Status::INDEX_DELETED);
            increment(&mut staged_ren, s.status(), Status::INDEX_RENAMED);
            // Unstaged files.
            increment(&mut unstaged_new, s.status(), Status::WT_NEW);
            increment(&mut unstaged_mod, s.status(), Status::WT_MODIFIED);
            increment(&mut unstaged_del, s.status(), Status::WT_DELETED);
            increment(&mut unstaged_ren, s.status(), Status::WT_RENAMED);
            // Conflicts
            increment(&mut conflicts, s.status(), Status::CONFLICTED);
        }

        // Print status changes of staged files in green, unstaged in cyan.
        print!("{}", Green.bold().paint(format_counts(staged_new, staged_mod, staged_del, staged_ren)));
        print!("{}", Cyan.bold().paint(format_counts(unstaged_new, unstaged_mod, unstaged_del, unstaged_ren)));

        // Print number of conflicts in red.
        if conflicts > 0
        {
            let mut conflict_str = String::new();
            append_if_non_zero(&mut conflict_str, 'C', conflicts);
            print!("{} ", Red.bold().paint(conflict_str));
        }
    }
}

/// If status 's' intersects with type 't', increment 'val', otherwise do nothing.
fn increment(val: &mut i32, s: Status, t: Status)
{
    if s.intersects(t)
    {
        *val += 1;
    }
}

/// Format the new/modified/deleted/renamed status counts into a string.
/// Each count is only included if it is non-zero.
fn format_counts(new: i32, mods: i32, del: i32, ren: i32) -> String
{
    let mut formatted = String::new();
    if new > 0 || mods > 0 || del > 0 || ren > 0
    {
        append_if_non_zero(&mut formatted, '+', new);
        append_if_non_zero(&mut formatted, '~', mods);
        append_if_non_zero(&mut formatted, '-', del);
        append_if_non_zero(&mut formatted, 'R', ren);
    }
    // Return the string.
    formatted
}

/// If val is non-zero than append char+val to the string, otherwise leave unmodified.
fn append_if_non_zero(s: &mut String, c: char, val: i32)
{
    if val != 0
    {
        *s = format!("{s}{c}{val} ");
    }
}

