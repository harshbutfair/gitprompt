# Gitprompt

This program prints a git summary status for the current directory, which
is intended to be included as part of a bash prompt. It is my first Rust program.

## Building

Like all Rust programs, build the debug version with `cargo build` and the release
version with `cargo build --release`.

## Example prompt

    export PS1="
    \t \[\e[1;32m\]\h\[\e[m\]:\[\e[1;33m\]\w\[\e[m\] \`/c/dev/gitprompt/target/release/gitprompt.exe\`
    \$ "
